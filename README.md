# TGUI Theme Builder

This tool supports building GUI sprite sheets from individual source images, and contains syntactic sugar to make 9-slices easier

Required images are defined using the following syntax:

```
Texture = ${imagename};
// Define a 9-slice just by a margin from the edge of the image
TextureHover = ${imagename:7};
```

The image by that name will be found and added to the sprite sheet. The same image can be referenced multiple times and only appear once. The output of the tool will look like this:

```
Texture = "themename.png" Part(2, 2, 21, 60);
// Define a 9-slice just by a margin from the edge of the image
TextureHover = "themename.png" Part(2, 2, 21, 60) Middle(7, 7, 7, 46);
```

License: zlib

## Use in Continuous Integration

This code will download this tools source code, and build a given theme's source code

```bash
git clone https://gitlab.com/rubenwardy/tgui-theme-builder
tgui-theme-builder/build_archive.sh nanogui.style
mv build/theme.zip theme.zip
```

Here's an example `.gitlab-ci.yml`:

```yml
image: python:3.7

before_script:
  - apt-get update
  - apt-get install zip

build:
  stage: build
  script:
    - git clone https://gitlab.com/rubenwardy/tgui-theme-builder
    - tgui-theme-builder/build_archive.sh nanogui.style
    - mv build/theme.zip theme.zip
  artifacts:
    paths:
    - theme.zip
  only:
    - master
```

## Use in CMake

```cmake
file(GLOB GUISources ${CMAKE_SOURCE_DIR}/assets/gui/source/*)
add_custom_target(prebuild ALL
	COMMAND utils/prebuild.sh
	WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
	DEPENDS ${GUISources}
	BYPRODUCTS ${CMAKE_SOURCE_DIR}/assets/gui/rvwp.style ${CMAKE_SOURCE_DIR}/assets/gui/rvwp.png)
```

where `prebuild.sh` may be something similar to:

```bash
#!/bin/bash

set -e

python3 -mvenv /tmp/rvwpenv
source /tmp/rvwpenv/bin/activate
pip install PyTexturePacker

pack=$PWD/utils/pack.py

pushd assets/gui/source
python $pack rvwp.style
mv build/*.{png,style} ..
rm -r build
popd
```

#!/bin/bash

if [[ -x "$1" ]]; then
	echo "Usage: source.style"
fi

set -e

# Set up venv
python3 -mvenv /tmp/rvwpenv
source /tmp/rvwpenv/bin/activate
pip install PyTexturePacker

# Pack
dir=$(dirname "${BASH_SOURCE[0]}")
pack=$dir/pack.py
python $pack $1

# Package
pushd build
rm *.plist
zip -r theme.zip *
popd
